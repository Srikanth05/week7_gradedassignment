package com.user.registration;
import java.sql.*;


public class ConnectionFile {
    private static Connection conn;
    
    public static Connection getConnection(){
		try {
			if(conn==null) {
				Class.forName("com.mysql.cj.jdbc.Driver");
				//connecting to database
				conn=(Connection)DriverManager.getConnection("jdbc:mysql://localhost:3306/bookdatabase?autoReconnect=true&useSSL=false","root","admin");
				System.out.println("database connected successfully\n");
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return conn;
    }
}

