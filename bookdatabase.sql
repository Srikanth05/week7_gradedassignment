create database  week_seven;
use week_seven;
CREATE TABLE books(
  book_id        INT NOT NULL, 
  title          VARCHAR(255) NOT NULL, 
  total_pages    INT NULL, 
  rating         DECIMAL(4, 2) NULL, 
  isbn           int not NULL, 
  PRIMARY KEY(book_id)
);

INSERT INTO BOOKS VALUES(1,"Algoritm of data and Architecture",450,3.5,249415),
(2,"Quore and Core of Java",450,4.5,452451),
(3,"Database System Concepts",350,3.5,145451),
(4,"Compiler Design",250,4.6,142451),
(5,"Pattern Classification",600,4.1,152451),
(6,"Fundamental Database System",450,4.2,222451),
(7,"Structured Computer Organization",220,4.3,111451),
(8,"Operating System",340,4.4,456451),
(9,"Microprocessor",680,4.6,145789),
(10,"Life with java",740,4.5,452151),


select * from books;
create table users (
   user_id 	   int not null auto_increment,
   name        varchar(45) not null,
   email 	   varchar(45) not null,
   password    varchar(45) not null,
   primary key(user_id)
);

insert into users values(1,"srikanth","srikanth@gmail.com","srikanth123");

select * from users;
